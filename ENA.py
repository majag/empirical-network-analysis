import networkx as nx
import matplotlib, numpy, community
import matplotlib.pyplot as plt
from operator import itemgetter
from scipy.cluster import hierarchy
from scipy.spatial import distance

graph = nx.read_weighted_edgelist("US_largest500_airportnetwork.txt")

file = open ("USair.txt", 'a');

#calculationg the number of nodes
N = str(len(graph))

#calculating the number of edges
K = str(graph.size())

#calculating average degree
k = str(nx.average_degree_connectivity(graph))

k2 = str((2*int(K))/int(N))

#finding cliques
cl = list(nx.find_cliques(graph))

#number of connected components
conn_comp = str(nx.number_connected_components(graph))

#avg shortest path
avg_shortest_path = str(nx.average_shortest_path_length(graph))

#diameter
diameter = str(nx.diameter(graph))

#clustering coefficients
clust_coef_loc = str(nx.clustering(graph))
clust_coef_avg = str(nx.average_clustering(graph))

#centrality measures
dc = nx.degree_centrality(graph)
degree_centrality = str(sorted(dc.items(), key = itemgetter(1), reverse = True))

cc = nx.closeness_centrality(graph)
closeness_centrality = str(sorted(cc.items(), key = itemgetter(1), reverse = True))

bc = nx.betweenness_centrality(graph)
betweenness_centrality = str(sorted(bc.items(), key = itemgetter(1), reverse = True))

ec = nx.eigenvector_centrality(graph)
eigenvector_centrality = str(sorted(ec.items(), key = itemgetter(1), reverse = True))

#Writing data
file.write("___________________________________________________________________________________\n\n")
file.write("US air transportation network analysis\n")
file.write("___________________________________________________________________________________\n\n")
file.write("number of nodes N = " + N + "\n")
file.write("number of edges K = " + K + "\n")
file.write("avg degree <k> = " + k + "\n")
file.write("<k> = " + k2 + "\n\n")
file.write("no of connected components = " + conn_comp + "\n")

file.write("\n\naverage shortest path = " + avg_shortest_path + "\n")    
file.write("diameter = " + diameter + "\n\n")
file.write("clustering coefficient = " + clust_coef_loc + "\n\n")
file.write("average clustering coefficient = " + clust_coef_avg + "\n\n")

file.write("___________________________________________________________________________________\n\n")
file.write("centrality measures (max-min)\n")
file.write("___________________________________________________________________________________\n\n")

file.write("degree centrality = " + degree_centrality + "\n\n")
file.write("closeness centrality = " + closeness_centrality + "\n\n")
file.write("betweenness centrality = " + betweenness_centrality + "\n\n")
file.write("eigenvector centrality = " + eigenvector_centrality + "\n\n\n")

#Generating ER graph
g1 = nx.erdos_renyi_graph(500, 0.024)
L = str(nx.average_shortest_path_length(g1))
C = str(nx.average_clustering(g1))
file.write("___________________________________________________________________________________\n\n")
file.write("Data of Erdos-Reny graph (500 nodes, p = 0.024 => 2958 edges)\n")
file.write("___________________________________________________________________________________\n\n")
file.write("average shortest path L = " + L + "\n")
file.write("clustering coefficient C = " + C + "\n\n")
file.write("From the results we can see that our network is indeed a small world network")

#Finding communities
file.write("\n\n___________________________________________________________________________________\n\n")
file.write("Finding Communities\n")
file.write("___________________________________________________________________________________\n\n")
file.write("Cliques: \n\n" + str(cl) + "\n\n\n")
file.write ("Largest clique = " + str(nx.graph_clique_number(graph)) + "\n\n")
file.write ("No of maximal cliques = " + str(nx.graph_number_of_cliques(graph)) + "\n\n")

#k-cores
file.write ("\n\nK-cores \n\n")
file.write (str(list(nx.k_core(graph))))
file.write ("\n\nK-cores with node degree 10 or higher \n\n")
file.write (str(list(nx.k_core(graph, 10))))

# Hierarchial grouping, bottom-up

path_length = nx.all_pairs_shortest_path_length(graph)
n = len(graph.nodes())
distances = numpy.zeros((n,n))
for u,p in path_length.items():
    for v,d in p.items():
        distances[int(u)-1][int(v)-1] = d
sd = distance.squareform(distances)

hier = hierarchy.average(sd)
file.write("\n\nHierarchial grouping, bottom-up \n\n" + str(hier) + "\n\n")
hierarchy.dendrogram(hier)
matplotlib.pylab.savefig("dendrogram.png")

#Louvains algorithm
file.write("___________________________________________________________________________________\n\n")
file.write("Louvains algorithm \n")
file.write("___________________________________________________________________________________\n")

#first compute the best partition
partition = community.best_partition(graph)

#drawing
plt.figure()
size = float(len(set(partition.values())))
pos = nx.spring_layout(graph)
count = 0.
for i in set(partition.values()) :
    file.write ("\n\nCommunity " + str(i) + "\n\n")
    members = list_nodes = [nodes for nodes in partition.keys() if partition[nodes] == i]
    file.write (str(members))
    
    count = count + 1.
    nx.draw_networkx_nodes(graph, pos, list_nodes, node_size = 20,
                                node_color = str(count / size))


nx.draw_networkx_edges(graph, pos, alpha=0.5)
plt.savefig('louvain.png')

#Assortativity
file.write("\n\n___________________________________________________________________________________\n\n")
file.write("\nAssortativity\n\n")
file.write(str(nx.degree_assortativity_coefficient(graph)))
file.write("\n\n\Assortativity using Pearson function\n\n")
file.write(str(nx.degree_pearson_correlation_coefficient(graph)))


file.close()

#Graph visualization

nx.draw(graph)
plt.savefig("graph1.png")
plt.figure()
nx.draw(g1)
plt.show()
plt.savefig("graph2.png")
